package com.example.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.splash.ui.login.LoginActivity;

public class SplashActivity extends AppCompatActivity {
        //Constants de tipo entero que representa el tiempo en milisegundos que se muestra nuestra actividad al usuario
    static int TIMEOUT_MILLIS = 5000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       //Asigna el archivo XML que va a ligarse como diseño de pantalla
        setContentView(R.layout.activity_splash);
       //Quitamos la action bar de la pantalla splash
        getSupportActionBar().hide();
        //lA CLASE handler permite enviar objetos de tipo Runnable
        //Un abjeto de tipo runnable de el significado a un hilo de proceso
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //intent es la definicion abstracta de una operacion a realizar
                //Puede ser usdo para una activity, brodcast reciver, servucuis

                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                //Finaliza esta activity
                finish();
            }
        }, TIMEOUT_MILLIS);
    }
}